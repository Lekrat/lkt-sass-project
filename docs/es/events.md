# LKT Events

Controla los eventos a través de las media queries.

## Módulos relacionados

- [LKT Media](./media-queries.md)

## Api

### Mixins

#### lkt-hover

**STATUS: STABLE**
**CONTENT-BLOCK: YES**

Evento hover.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $media-query | Alias de [LKT Media](./media-queries.md) | '' |

##### Ejemplos
```
.sample-1 {
    color: red;
    
    @include lkt-hover(lg){
        color: blue;
    }
}
```

#### lkt-touch

**STATUS: TESTING**
**CONTENT-BLOCK: YES**

Evento touch.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $media-query | Alias de [LKT Media](./media-queries.md) | '' |

##### Ejemplos
```
.sample-1 {
    color: red;
    
    @include lkt-touch(lg){
        color: blue;
    }
}
```