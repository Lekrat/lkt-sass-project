# LKT Media

## Configuración

La configuración general está detallada en [LKT Config](./config.md#media-queries).

## Qué hay en la caja

LKT Media incluye las siguientes media queries por defecto:

- ie10
- ie11
- moz
- chrome
- safari9
- 120dpi
- 124dpi
- 144dpi
- 192dpi
- portrait
- landscape

**Nota 1:** El string vacío es usado para identificar la ausencia de media query.
Esta característica es usada por otros módulos para generar CSS externo a las media queries.

**Nota 2:** La media query para firefox detecta las versiones 4+

**Nota 3:** La media query para chrome detecta las versiones 29+

## Agregar más alias de media queries

Para agregar nuevos alias, simplemente usa la función lkt-media.

## Api

### Funciones

#### lkt-media

**STATUS: STABLE**

Agrega un nuevo alias de media query.

A la hora de agregar nuevos alias, ten en cuenta que el nombre será usado por los siguientes módulos para generar selectores CSS.

- [LKT Displays](./displays.md)
- [LKT Grid](./grid.md)
- [LKT Gap](./gap.md)

| Parámetros | Descripción | Defecto |
|---|---|---|
| $media-name | El alias de la media query. | '' |
| $rules | Las condiciones de la media query | '' |

##### Ejemplos

```
// Ejemplo de un sistema mobile-first
$add: lkt-media(medium, '(min-width: 720px)');
$add: lkt-media(lg, '(min-width: 1280px)');
$add: lkt-media(xlg, '(min-width: 1920px)');

// Ejemplo de un sistema large-first
$add: lkt-media(medium, '(max-width: 1280px)');
$add: lkt-media(sm, '(max-width: 720px)');
$add: lkt-media(tn, '(max-width: 480px)');

// Ejemplo con aspect ratio
$add: lkt-media(aspect-2-3, '(min-aspect-ratio: 2) and (max-aspect-ratio: 3)');

// Otro ejemplo mobile first más completo
$add: lkt-media(xs, '(min-width: 576px)');
$add: lkt-media(only-xs, '(min-width: 576px) and (max-width: 767px)');

$add: lkt-media(to-sm, '(max-width: 767px)');
$add: lkt-media(sm, '(min-width: 768px)');
$add: lkt-media(only-sm, '(min-width: 768px) and (max-width: 991px)');

$add: lkt-media(to-md, '(max-width: 991px)');
$add: lkt-media(md, '(min-width: 992px)');
$add: lkt-media(md-only, '(min-width: 992px) and (max-widht: 1199px)');

$add: lkt-media(to-lg, '(max-widht: 1199px)');
$add: lkt-media(lg, '(min-widht: 1200px)');
```

### Mixins

#### lkt-media

**STATUS: STABLE**
**CONTENT-BLOCK: YES**

Incluye los media queries definidos con la función homónima.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $media-name | El alias de la media query. | '' |
| $media-type | El tipo de media | lkt-config(media-default-target) |

##### Ejemplos

```
.sample-1 {
    @include lkt-media(medium) {
        color: red;
    }
    
    @include lkt-media(large, 'print') {
        color: orange;
    }
}
```