# LKT Displays

Maneja el display de los elementos a través de las media queries.

Genera tus propios displays para tus propias media queries.

## Módulos relacionados

- [LKT Media](./media-queries.md)

## Api

### Mixins

#### lkt-displays

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera una serie de selectores CSS para utilizar a modo de helpers.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $css-prefix | Prefijo del selector CSS. | 'display' |
| $display-list | Una lista de SASS con los displays que quieres generar | () |
| $media-queries | Una lista de SASS con los alias de los media queries | () |


##### Ejemplos
```
// Declaramos una media query de ejemplo
$add: lkt-media-add(only-sm, false, 640px);

// Generate helpers (empty media query is already defined)
@include lkt-displays(is, none block flex inline-flex, '' only-sm);
```

Esto generará el código siguiente:

```
.is-none {
  display: none;
}

.is-block {
  display: block;
}

.is-flex {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.is-inline-flex {
  display: -webkit-inline-box;
  display: -ms-inline-flexbox;
  display: inline-flex;
}

@media screen and (max-width: 320px) {
  .is-none-only-sm {
    display: none;
  }

  .is-block-only-sm {
    display: block;
  }

  .is-flex-only-sm {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
  }

  .is-inline-flex-only-sm {
    display: -webkit-inline-box;
    display: -ms-inline-flexbox;
    display: inline-flex;
  }
}
```