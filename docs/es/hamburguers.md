# LKT Hamburgers

Icono de hamburguesa rápido.

## Configuración

La configuración general está detallada en [LKT Config](./config.md#hamburgers).

## Estructura HTML
Todos los diferentes tipos de icono tienen la misma estructura HTML.

```
<button class="sample-1" type="button">
    <span class="box"><span class="inner"></span></span>
    <span class="label">Menu</span>
</button>
```
Para ver el icono activo, hay que agregar al botón la clase "is-active".

## Api

### Mixins

#### lkt-hamburger-3dx

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X haciendo un giro sobre el eje X.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-3dx();
}
.sample-2 {
    @include lkt-hamburger-3dx(true);
}
```

#### lkt-hamburger-3dxy

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X haciendo un giro sobre los ejes X e Y.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-3dxy();
}
.sample-2 {
    @include lkt-hamburger-3dxy(true);
}
```

#### lkt-hamburger-3dy

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X haciendo un giro sobre el eje Y.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-3dy();
}
.sample-2 {
    @include lkt-hamburger-3dy(true);
}
```

#### lkt-hamburger-arrow

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en flecha.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-arrow();
}
.sample-2 {
    @include lkt-hamburger-arrow(true);
}
```

#### lkt-hamburger-arrow-alt

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en flecha.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-arrow-alt();
}
.sample-2 {
    @include lkt-hamburger-arrow-alt(true);
}
```

#### lkt-hamburger-arrow-turn

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en flecha.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-arrow-turn();
}
.sample-2 {
    @include lkt-hamburger-arrow-turn(true);
}
```

#### lkt-hamburger-boring

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en flecha (sin animación).

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-boring();
}
```

#### lkt-hamburger-collapse

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en flecha.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-collapse();
}
.sample-2 {
    @include lkt-hamburger-collapse(true);
}
```

#### lkt-hamburger-elastic

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X.

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-elastic();
}
```

#### lkt-hamburger-emphatic

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-emphatic();
}
.sample-2 {
    @include lkt-hamburger-emphatic(true);
}
```

#### lkt-hamburger-minus

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en un guión.

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-minus();
}
```

#### lkt-hamburger-slider

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-slider();
}
.sample-2 {
    @include lkt-hamburger-slider(true);
}
```

#### lkt-hamburger-spin

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-spin();
}
.sample-2 {
    @include lkt-hamburger-spin(true);
}
```

#### lkt-hamburger-spring

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-spring();
}
.sample-2 {
    @include lkt-hamburger-spring(true);
}
```

#### lkt-hamburger-squeeze

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X.

#### lkt-hamburger-stand

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-stand();
}
```

#### lkt-hamburger-vortex

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Cuando está activa, se convierte en X.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $is-reverse | Genera el efecto a la inversa | false |

##### Ejemplos

```
.sample-1 {
    @include lkt-hamburger-vortex();
}
.sample-2 {
    @include lkt-hamburger-vortex(true);
}
```