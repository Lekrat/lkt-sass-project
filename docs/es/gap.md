# LKT Gap

## Api

### Mixins

#### lkt-gap

**STATUS: STABLE**
**CONTENT-BLOCK: YES**

Genera los helpers para utilizar gap.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $name | Nombre que se utilizará para el selector CSS | 'gap-15' |
| $size | Tamaño del gap | 15px |
| $media-queries | Una lista de Sass con los alias de [LKT Media](./media-queries.md). Se generarán los selectores necesarios para funcionar en todas esas media queries.  | ('') |

```
@include lkt-gap('gap-30', 30px, ('' sm));
@include lkt-gap('gap-10', lkt-rem(10px), ('' sm));
```

#### lkt-gap-horizontal

**STATUS: STABLE**
**CONTENT-BLOCK: YES**

Similar a lkt-gap, pero solo crea relleno de forma horizontal.

#### lkt-gap-vertical

**STATUS: STABLE**
**CONTENT-BLOCK: YES**

Similar a lkt-gap, pero solo crea relleno de forma vertical.