# Functions
## String

| Function | Description | Params |
|---|---|---|
| lkt-concat | Concat strings | $glue, $strings... |
| lkt-str-replace | Replaces in string | $string, $search, $replace |
| lkt-to-fixed | Convert a float to string | $float, $digits: 2 |
| lkt-to-string | Cast to string | $value |

## List

| Function | Description | Params |
|---|---|---|
| lkt-contains | Check if the given index exists in list | $list, $var |
| lkt-list-nth-delete | Delete item from list at given position | $list, $n |

## Units

| Function | Description | Params |
|---|---|---|
| lkt-is-absolute-length | Check if unit in (cm, mm, in, px, pt, pc) | $value |
| lkt-is-angle | Check if unit in (deg, rad, grad, turn) | $value |
| lkt-is-frequency | Check if unit in (Hz, kHz) | $value |
| lkt-is-integer | Check if integer | $value |
| lkt-is-length | Check if lkt-is-relative-length or lkt-is-absolute-length | $value |
| lkt-is-percentage | Check if unit in (%) | $value |
| lkt-is-number | Check if number | $value |
| lkt-is-relative-length | Check if unit in (em, ex, ch, rem, vw, vh, vmin, vmax) | $value |
| lkt-is-resolution | Check if unit in (dpi, dpcm, dppx) | $value |
| lkt-is-time | Check if unit in (ms, s) | $value |
| lkt-strip-unit | Remove unit. (1px => 1) | $value |

# Mixins
| Function | Description | Params |
|---|---|---|
| lkt-force-hardware-acceleration | Enables hardware acceleration | bool $use-transform |
| lkt-text-overflow-ellipsis | text-overflow: ellipsis shorthand | --- |
| lkt-out-of-bounds | Escape an element out of container size | --- |