# LKT Aligns

El objetivo de este módulo es permitir alinear elementos de forma sencilla.

## Api

### Mixins

#### lkt-align

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Alinea con una transformación.

Establece left:$x, top: $y para después hacer una transformación negativa.

Usa este mixin en combinación con la propiedad position.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $x | Distancia respecto al eje x | false |
| $y | Distancia respecto al eje y | false |

##### Ejemplos
```
.sample-1 {
    @include lkt-align(50%, 50%);
}
.sample-2 {
    @include lkt-align(false, 50%);
}
.sample-3 {
    @include lkt-align(50%, false);
}
```


