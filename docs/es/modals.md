# LKT Modals

Módulo para la gestión de modales html.

## Configuración

La configuración general está detallada en [LKT Config](./config.md#modales).

## Estructura HTML
La estrctura básica del HTML es la siguiente:

```
<div class="modal">
    <div class="back"></div>
    <div class="inner"></div>
</div>
```
Para que el modal sea visible, hay que agregar al contenedor la clase "is-open".

## Api

### Mixins

#### lkt-modal

**STATUS: UNSTABLE**

**CONTENT-BLOCK: NO**

Genera los estilos para el modal.

Los modales aparecen centrados vertical y horizontalmente.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $content-width | Ancho máximo del contenido | 50% |
| $content-height | Alto máximo del contenido | 50% |
| $animation | Animación de apertura. Valores: default, theatre, none | default |

##### Ejemplos
```
.sample-1 {
    @include lkt-modal(50%, 50%);
}
.sample-2 {
    @include lkt-modal(80%, 50%, theatre);
}
```


