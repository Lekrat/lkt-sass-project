# LKT Presets

Este módulo se encarga de compatibilizar con otros frameworks.

## Preset: Bootstrap

Incluye los siguientes mixins:

### lkt-preset-bootstrap-media-queries

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Agrega los siguientes alias de media queries para trabajar con las mismas medias que bootstrap.

| Alias de [LKT Media](./media-queries.md) | min-width | max-width |
|---|---|---|
| xs | - | 575px |
| to-sm | - | 575px |
| sm | 576px | - |
| only-sm | 576px | 767px |
| to-md | - | 767px |
| md | 768px | - |
| only-md | 768px | 991px |
| to-lg | - | 991px |
| lg | 992px | - |
| only-lg | 992px | 1199px |
| to-xlg | - | 1199px |
| xlg | 1200px | - |

#### Ejemplos

```
@include lkt-preset-bootstrap-media-queries();
```

### lkt-preset-bootstrap-grid

**STATUS: TESTING**

**CONTENT-BLOCK: NO**

Genera los selectores CSS para usar un sistema de columnas similar a bootstrap.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $grid-media-queries | Lista con las media queries con que se generará | ('' sm md) |

#### Ejemplos

```
@include lkt-preset-bootstrap-grid();
@include lkt-preset-bootstrap-grid('' md);
@include lkt-preset-bootstrap-grid('' md lg);
```