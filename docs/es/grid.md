# LKT Grid

Tu propio sistema de columnas.

## Configuración

La configuración general está detallada en [LKT Config](./config.md).

## Api

### Mixins

#### lkt-clear-fix

**STATUS: STABLE**

Agregar un pseudo elemento para forzar el truco del clear fix.

Complemento para el uso de float.

##### Ejemplos

```
.sample-1 {
    @include lkt-clear-fix;
}
```

#### lkt-grid-flex

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera un grid diseñado en torno a flexbox.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $row-name | Nombre que se utilizará para formar el selector de fila | 'row' |
| $column-name | Nombre que se utilizará para formar el selector de columna, ordenado, push y pull | 'col' |
| $amount-of-columns | Cantidad de columnas del sistema. Note que puede ser un número o varios (ver ejemplos) | 12 |
| $media-queries | Una lista de Sass con los alias de [LKT Media](./media-queries.md). Se generarán los selectores necesarios para funcionar en todas esas media queries. | () |
| $include-order | Indica si generar los selectores de ordenado | lkt-config(grid-include-order) |
| $include-modifiers | Indica si generar los selectores para modificar las propiedades flex | lkt-config(grid-include-modifiers) |
| $include-push-and-pull | Indica si generar los selectores para empujar y tirar de las columnas | lkt-config(grid-include-push-and-pull) |

##### Ejemplos

```
@include lkt-grid-flex(my-row, my-col, 2 12, '' sm md);
@include lkt-grid-flex(my-row, column, 1 8 24, '' xs lg, false, false, false);
```

Los selectores generados podrán ser utilizados por una estructura HTML similar a esta:

```
<div class="my-row">
    <div class="my-col-1-2 my-col-12-12-only-sm">
        Lorem Ipsum
    </div>
    <div class="my-col-3-12 my-col-6-12-only-sm">
        Dolor
    </div>
    <div class="my-col-3-12 my-col-1-1-only-sm">
        Sit Amet
    </div>
</div>
```

##### Orden flex

Si se han generado los modificadores de orden, se tomará el número de columnas más alto y se agregará un selector ```$columnName-order-$i``` por cada ancho de columna.

Además, se agregará el selector ```$columnName-order-last```.

##### Modificadores flex

Si hemos optado por agregar los modificadores flex, se generarán los siguientes helpers para cada media query (más información sobre la sintaxis del selector en [LKT Config](@todo:link))

```
.align-items-start {
  align-items: flex-start !important;
}

.align-items-end {
  align-items: flex-end !important;
}

.align-items-center {
  align-items: center !important;
}

.align-items-baseline {
  align-items: baseline !important;
}

.align-items-stretch {
  align-items: stretch !important;
}

.align-content-start {
  align-content: flex-start !important;
}

.align-content-end {
  align-content: flex-end !important;
}

.align-content-center {
  align-content: center !important;
}

.align-content-between {
  align-content: space-between !important;
}

.align-content-around {
  align-content: space-around !important;
}

.align-content-stretch {
  align-content: stretch !important;
}

.align-self-auto {
  align-self: auto !important;
}

.align-self-start {
  align-self: flex-start !important;
}

.align-self-end {
  align-self: flex-end !important;
}

.align-self-center {
  align-self: center !important;
}

.align-self-baseline {
  align-self: baseline !important;
}

.align-self-stretch {
  align-self: stretch !important;
}

.flex-row {
  flex-direction: row !important;
}

.flex-column {
  flex-direction: column !important;
}

.flex-row-reverse {
  flex-direction: row-reverse !important;
}

.flex-column-reverse {
  flex-direction: column-reverse !important;
}

.flex-wrap {
  flex-wrap: wrap !important;
}

.flex-nowrap {
  flex-wrap: nowrap !important;
}

.flex-wrap-reverse {
  flex-wrap: wrap-reverse !important;
}

.justify-content-start {
  justify-content: flex-start !important;
}

.justify-content-end {
  justify-content: flex-end !important;
}

.justify-content-center {
  justify-content: center !important;
}

.justify-content-between {
  justify-content: space-between !important;
}

.justify-content-around {
  justify-content: space-around !important;
}
```

#### lkt-grid-float

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera un grid diseñado en torno a float.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $row-name | Nombre que se utilizará para formar el selector de fila | 'row' |
| $column-name | Nombre que se utilizará para formar el selector de columna, ordenado, push y pull | 'col' |
| $amount-of-columns | Cantidad de columnas del sistema. Note que puede ser un número o varios (ver ejemplos) | 12 |
| $media-queries | Una lista de Sass con los alias de [LKT Media](./media-queries.md). Se generarán los selectores necesarios para funcionar en todas esas media queries. | () |
| $include-push-and-pull | Indica si generar los selectores para empujar y tirar de las columnas | lkt-config(grid-include-push-and-pull) |

##### Ejemplos

```
@include lkt-grid-float(my-row, my-col, 2 12, '' sm md);
@include lkt-grid-float(my-row, column, 1 8 24, '' xs lg, false, false, false);
```

Los selectores generados podrán ser utilizados por una estructura HTML similar a esta:

```
<div class="my-row">
    <div class="my-col-1-2 my-col-12-12-only-sm">
        Lorem Ipsum
    </div>
    <div class="my-col-3-12 my-col-6-12-only-sm">
        Dolor
    </div>
    <div class="my-col-3-12 my-col-1-1-only-sm">
        Sit Amet
    </div>
</div>
```