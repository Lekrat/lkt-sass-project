# LKT Overlays

Para generar de forma rápida capas de overlays.

## Api

### Mixins

#### lkt-overlay

**STATUS: TESTING**

**CONTENT-BLOCK: NO**

Genera un overlay.

El elemento sobre el que se utiliza es el contenedor del overlay.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $overlay-selector | Nombre de la clase | overlay |
| $overlay-color-hex | Color de la capa, en hexadecimal | #000000 |
| $overlay-opacity | Opacidad de la capa, de 0 a 1 | .2 |

##### Ejemplos
```
.sample-1 {
    @include lkt-overlay();
}
.sample-2 {
    @include lkt-overlay(my-overlay, #ff0000, .5);
}
```


