
# Api

## Params
| Param | Description |
|---|---|
|$name|Name|
|$width|Width|
|$height|Height|

## Mixins

This module provides you the following mixins:

| Mixin | Description | Params |
|---|---|---|
| lkt-multimedia-fit | Generates CSS based on configured aspect ratio | $name |
| lkt-multimedia-in | Generates CSS based on configured aspect ratio | $name |
| lkt-multimedia-out | Generates CSS based on configured aspect ratio | $name |
| lkt-multimedia-fit-custom | Generates CSS reserving the space given | $size |
| lkt-multimedia-in-custom | Generates CSS reserving the space given | $name |
| lkt-multimedia-out-custom | Generates CSS reserving the space given | $size |
| lkt-multimedia-parallax | You only has to assign background-image property | $name |
| lkt-multimedia-parallax-custom | You only has to assign background-image property | $size |

## Functions

| Function | Description | Params |
|---|---|---|
| lkt-multimedia-add | Add a multimedia aspect ratio | $name, $width, $height |
| lkt-multimedia-add-custom | Add a multimedia aspect ratio | $name, $value |

## Predefined aspect rations:

- 1:1
- 2:3
- 3:2
- 3:4
- 4:3
- 4:5
- 5:4
- 9:16
- 9:21
- 9:32
- 10:16
- 16:9
- 16:10
- 21:9
- 32:9