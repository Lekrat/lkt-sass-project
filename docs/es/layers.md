# LKT Layers

## Configuración

### Agregar nuevas capas

Puedes agregar nuevas capas a la configuración usando la función [lkt-layer-add](#lkt-layer-add).

## Qué hay en la caja

LKT Layer incluye las siguientes media queries por defecto:

- modal: 30

## Api

### Funciones

#### lkt-layer

**STATUS: STABLE**

Agrega una nueva capa a la configuración.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $name | El nombre de la capa. Un alias interno para LKT Layers. | '' |
| $value | El nivel de la capa (z-index). | 1 |

##### Ejemplos

```
$add: lkt-layer(default, 1);
$add: lkt-layer(modal, 10);
$add: lkt-layer(sample, 50);
$add: lkt-layer(something, 100);
```


### Mixins

#### lkt-layer

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera el código CSS de la capa.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $name | El nombre de la capa. Un alias interno para LKT Layers. | '' |

##### Ejemplos

```
.sample-1
{
    @include lkt-layer(sample);
}
```
