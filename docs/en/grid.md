# Flex Grid

## Api

### Params
| Param | Description |
|---|---|
| $row-name | CSS selector for the row |
| $column-name | CSS selector for the column |
| $amount-of-columns | A list with all the column system which will be generated. Samples: 8 12; 12; 1 2 3 4 5; |
| $media-queries | A list with all the media breakpoints (empty query must be included if you want non-media selectors) [more](https://gitlab.com/Lekrat/lkt-sass-project/blob/master/docs/media-queries.md) |
| $include-order | If true, order selectors will be generated. Default true |
| $include-modifiers | If true, flex modifier selectors will be generated. Default true |
| $include-push-and-pull | If true, push and pull modifiers will be generated. Default true |

### Mixins

| Mixin | Description | Params |
|---|---|---|
| lkt-grid-flex | Generates your own grid system. Your amount of columns. | $row-name, $column-name, $amount-of-columns, $media-queries, $include-order, $include-modifiers, $include-push-and-pull |

### Selector syntax

| Param | Description |
|---|---|
| row | $rowName |
| column | $columnName-$columnWidth-$columnAmount-$mediaQuery |

### Sample

```
// Declare media queries
$add: lkt-media(only-sm, false, 640px);

// Generate helpers (empty media query is already defined)
@include lkt-grid-flex(my-row, my-col, 2 12, '' only-sm md);
```

And you will be able to use the next html structure:

```
<div class="my-row">
    <div class="my-col-1-2 my-col-12-12-only-sm">
        Lorem Ipsum
    </div>
    <div class="my-col-3-12 my-col-6-12-only-sm">
        Dolor
    </div>
    <div class="my-col-3-12 my-col-1-1-only-sm">
        Sit Amet
    </div>
</div>
```

### Flex modifiers

When the grid it's generated, it's also come with the following modifiers (for each media query, just append "-$mediaQueryName" at the end):

```
.align-items-start {
  align-items: flex-start !important;
}

.align-items-end {
  align-items: flex-end !important;
}

.align-items-center {
  align-items: center !important;
}

.align-items-baseline {
  align-items: baseline !important;
}

.align-items-stretch {
  align-items: stretch !important;
}

.align-content-start {
  align-content: flex-start !important;
}

.align-content-end {
  align-content: flex-end !important;
}

.align-content-center {
  align-content: center !important;
}

.align-content-between {
  align-content: space-between !important;
}

.align-content-around {
  align-content: space-around !important;
}

.align-content-stretch {
  align-content: stretch !important;
}

.align-self-auto {
  align-self: auto !important;
}

.align-self-start {
  align-self: flex-start !important;
}

.align-self-end {
  align-self: flex-end !important;
}

.align-self-center {
  align-self: center !important;
}

.align-self-baseline {
  align-self: baseline !important;
}

.align-self-stretch {
  align-self: stretch !important;
}

.flex-row {
  flex-direction: row !important;
}

.flex-column {
  flex-direction: column !important;
}

.flex-row-reverse {
  flex-direction: row-reverse !important;
}

.flex-column-reverse {
  flex-direction: column-reverse !important;
}

.flex-wrap {
  flex-wrap: wrap !important;
}

.flex-nowrap {
  flex-wrap: nowrap !important;
}

.flex-wrap-reverse {
  flex-wrap: wrap-reverse !important;
}

.justify-content-start {
  justify-content: flex-start !important;
}

.justify-content-end {
  justify-content: flex-end !important;
}

.justify-content-center {
  justify-content: center !important;
}

.justify-content-between {
  justify-content: space-between !important;
}

.justify-content-around {
  justify-content: space-around !important;
}
```

# Float Grid

##  Api

### Mixins

| Mixin | Description | Params |
|---|---|---|
| lkt-clear-fix | Adds an after pseudo width the clearfix trick | --- |

# Grid Gap

##  Api

### Mixins

| Mixin | Description | Params |
|---|---|---|
| lkt-gap | Generate a fresh new gap selector. Put it in your row. | $name, $size, $media-query |
| lkt-gap-horizontal | Generate a fresh new gap selector. Put it in your row. | $name, $size, $media-query |
| lkt-gap-vertical | Generate a fresh new gap selector. Put it in your row. | $name, $size, $media-query |
