# LKT Config

## Aspectos básicos

Este módulo abarca toda la configuración de LKT Sass.

## Responsividad

Al configurar cualquier propiedad, el segundo parámetro indica el alias de [LKT Media](./media-queries.md) al que va asociado.

Esto implica que esos valores solo serán utilizados cuando estemos en esa media query.

Deja el segundo parámetro como ```false``` para configurar las propiedades por defecto (sin media query).

## Modo debug

Si está activo, LKT mostrará warnings cuando ocurra algo inesperado.

```
$c: lkt-config(debug, false, false);
```

## Tamaño del diseño

El tamaño del diseño es una propiedad estrechamente relacionada con [LKT Units](./units.md), pero merece un epígrafe aparte.

### Ancho del viewport

Establece el ancho del viewport del diseño.

Este valor se convierte en el valor de referencia cuando utilizamos lkt-vw, tanto como función o mixin, a menos que especifiquemos otro.

```
$c: lkt-config(design-viewport-width, false, 1280px);
```

### Alto del viewport

Establece el alto del viewport del diseño.

Este valor se convierte en el valor de referencia cuando utilizamos lkt-vw, tanto como función o mixin, a menos que especifiquemos otro.

```
$c: lkt-config(design-viewport-height, false, 720px);
```

## Fonts

### Default font-family

Establece la tipografía por defecto del proyecto.

Utilizado por [LKT Reset](./reset.md).

La tipografía configurada debe ser un alias de [LKT Fonts](./fonts.md).

```
$c: lkt-config(font-default, false, arial);
```

### Default font-size

Establece el tamaño base de la tipografía.

Utilizado por [LKT Reset](./reset.md).

```
$c: lkt-config(font-size, false, 16px);
```

### Default line-height

Establece el line-height base.

Utilizado por [LKT Reset](./reset.md).

```
$c: lkt-config(line-height, false, 1.425);
```

### Line height relativo

Indica si [LKT Font](./fonts.md) auto convertirá los line heights con unidades a relativos.

```
$c: lkt-config(font-relative-line-height, false, true);
```

### Tamaño de la fuente dependiente del viewport

Configura la fuente para crecer automáticamente en función del viewport.

```
$c: lkt-config(font-responsive-unit, false, vw);

// Además, podemos configurar las dimensiones máximas del viewport
$c: lkt-config(font-responsive-min-viewport-width, false, 320px);
$c: lkt-config(font-responsive-max-viewport-width, false, 1024px);
$c: lkt-config(font-responsive-min-viewport-height, false, false);
$c: lkt-config(font-responsive-max-viewport-height, false, false); 
```

Recuerda configurar también la [Unidad de conversión](#unidad-para-límite-de-crecimiento).


## Grid

### Generar modificadores

Cuando generamos un grid, tenemos la posibilidad de generar automáticamente una serie de selectores CSS auxiliares.

Estos selectores auxiliares son:

- Ordenado de columnas
- Alineado de columnas
- Push & pull de columnas

Cada uno de estos modificadores se configura por separado.

```
$c: lkt-config(grid-include-order, false, true);
$c: lkt-config(grid-include-modifiers, false, true);
$c: lkt-config(grid-include-push-and-pull, false, true);
```

### Formato de los selectores CSS

Los selectores de fila y columna tienen un formato personalizable.

Los valores por defecto son:

- row: ':n-:m'
- column: ':n-:i-:t-:m'
- order: ':n-:i-:t-:m'

En esas cadenas puedes poner lo que quieras, teniendo en cuenta que hay una serie de catacteres reservados.

| Carácter | Reemplazo |
|---|---|
| :i | Índice actual al que queremos acceder. Si es la tercera columna en un sistema de 12 columnas, :i sería 3. |
| :m | Media query actual. |
| :n | Nombre dado al generar el grid. Las columnas y los ordenados comparten el mismo nombre. |
| :t | Número total de columnas. |

```
$c: lkt-config(grid-name-row, false, ':n-:m');
$c: lkt-config(grid-name-column, false, ':n-:i-:t-:m');
$c: lkt-config(grid-name-order, false, ':n-:i-:t-:m');
```

## Hamburgers

Fork del [trabajo de Jonsuh](https://github.com/jonsuh/hamburgers).

```
$c: lkt-config(hamburger-padding-x, false, 15px);
$c: lkt-config(hamburger-padding-y, false, 15px);

// Ancho de las líneas del menú
$c: lkt-config(hamburger-layer-width, false, 40px);

// Alto de cada línea
$c: lkt-config(hamburger-layer-height, false, 4px);

// Espacio entre líneas
$c: lkt-config(hamburger-layer-spacing, false, 4px);

// Color de las líneas
$c: lkt-config(hamburger-layer-color, false, #000);

// Border radius de las líneas
$c: lkt-config(hamburger-layer-border-radius, false, 4px);

// Opacidad al hacer hover
$c: lkt-config(hamburger-hover-opacity, false, .7);

// Color de las líneas cuando está activo (selector .is-active)
$c: lkt-config(hamburger-active-layer-color, false, #000);

// Opacidad al hacer hover cuando está activo (selector .is-active)
$c: lkt-config(hamburger-active-hover-opacity, false, #000);
```


## Media Queries

### Tipo de media por defecto

Estable el tipo de media de una media query.

Entendemos "tipo de media" como esto: ```@media $tipo-de-media and (min-width: 100px)```.

Los tipos de media disponibles (que entienden los navegadores) son:

- braille
- embossed
- handheld
- print
- projection
- screen
- speech
- tty
- tv

Utilizado por [LKT Media](./media-queries.md).

```
$c: lkt-config(media-default-target, false, 'all');
```

## Modo esquema

El modo esquema permite combinar selectores CSS en uno solo.

Por defecto viene activado para todos los módulos.

Puede ser desactivado sobrescribiendo la configuración.

```
$c: lkt-config(schema-mode-aligns, false, true);
$c: lkt-config(schema-mode-font, false, true);
$c: lkt-config(schema-mode-layers, false, true);
$c: lkt-config(schema-mode-wrap, false, true);
```

## Media queries para modo esquema

Indica las media queries en las que usará el modo esquema.

```
$c: lkt-config(schema-media-queries, false, (''));
```

## Modales

### Duración de las animaciones de apertura

```
$c: lkt-config(modal-inner-animation-time, false, 250ms);
$c: lkt-config(modal-back-animation-time, false, 2000ms);
```

### Layer por defecto
```
$c: lkt-config(modal-layer, false, modal);
```

### Opacidad final del background
```
$c: lkt-config(modal-back-opacity-final, false, .75);
```

## Scroll

### momentum-scroll-media

Configuración de la media query en la que se activará el momentum scroll (scroll suave para dispositivos móviles).

```
$c: lkt-config(momentum-scroll-media, false, false);
```

### scroll-main-direction

Indica la dirección principal en la que va a fluir la página.

Valores posibles: false, vertical

```
$c: lkt-config(scroll-main-direction, false, false);
```
 


## Units

### Redondeo de unidades

A la hora de convertir entre unidades, podemos especificar cuántos decimales deseamos dejar.

Como máximo, podrán aparecer 5 decimales.

Utilizado por [LKT Units](./units.md).

```
$c: lkt-config(unit-round-em, false, 2); // A la hora de convertir a em
$c: lkt-config(unit-round-rem, false, 2); // A la hora de convertir a rem
$c: lkt-config(unit-round-percent, false, 2); // A la hora de convertir a %
$c: lkt-config(unit-round-vh, false, 2); // A la hora de convertir a vh
$c: lkt-config(unit-round-vw, false, 2); // A la hora de convertir a vw
$c: lkt-config(unit-round-lh, false, 2); // A la hora de calcular line heights
```

### Factor de conversión a rem

Para convertir de px a rem, necesitamos utilizar un factor de conversión.

Por defecto es 16, pero puedes cambiarlo de esta forma:

```
$c: lkt-config(unit-factor-rem, false, 16);
```

Utilizado por [LKT Units](./units.md).

### Incluir los px al convertir

Es una opción para los mixins.

Si está configurado como true, además de mostrar el valor convertido se mostrará el valor en px.

Es útil cuando quieres depurar, aunque se recomienda desactivarlo para producción (aligera el CSS final).

```
$c: lkt-config(unit-include-px, false, true);
```

Utilizado por [LKT Units](./units.md).

### Unidad para límite de crecimiento

Indica en qué unidad se mostrará el crecimiento máximo para los mixins lkt-big-enough y lkt-small-enough.

```
$c: lkt-config(unit-enough, false, 'px');
```

Utilizado por [LKT Units](./units.md).

## Support

Activa el soporte para determinados vendors. Por defecto vienen todos a false.

```
$c: lkt-config(support-wordpress, false, false);
```