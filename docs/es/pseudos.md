# LKT Pseudos

Mixins para agilizar la declaración de pseudo-elementos.

## Api

### Mixins

#### lkt-after

**STATUS: STABLE**

**CONTENT-BLOCK: YES**

Declara un pseudo elemento de tipo after.

Si el parámetro $display es nulo, solo generará el content-block.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $display | Tipo de display | null |
| $position | Tipo de posicionado | initial |
| $content | Contenido inicial | '' |

##### Ejemplos

```
.sample-1 {
    position: relative;

    @include lkt-after(block, absolute, '+'){
        color: red;
    }

    @include lkt-hover(lg){
        @include lkt-after(){
            color: blue;
        }
    }
}
```

#### lkt-before

**STATUS: STABLE**

**CONTENT-BLOCK: YES**

Declara un pseudo elemento de tipo before.

Si el parámetro $display es nulo, solo generará el content-block.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $display | Tipo de display | null |
| $position | Tipo de posicionado | initial |
| $content | Contenido inicial | '' |

##### Ejemplos

```
.sample-1 {
    position: relative;

    @include lkt-before(block, absolute, '+'){
        color: red;
    }

    @include lkt-hover(lg){
        @include lkt-before(){
            color: blue;
        }
    }
}
```

#### lkt-pseudos

**STATUS: STABLE**

**CONTENT-BLOCK: YES**

Combina lkt-after y lkt-before.

Si el parámetro $display es nulo, solo generará el content-block.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $display | Tipo de display | null |
| $position | Tipo de posicionado | initial |
| $content | Contenido inicial | '' |

##### Ejemplos

```
.sample-1 {
    position: relative;

    @include lkt-pseudos(block, absolute, '+'){
        color: red;
    }
}
```
