# LKT Tools

El cajón de sastre para las esas grandes funcionalidades que no encajan en ningún módulo.

## Api

### Funciones

La mayoría de las funciones están más orientadas hacia la programación en Sass.

#### lkt-concat

**STATUS: STABLE**

Concatena cadenas de texto.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $glue | Texto con el que uniremos las cadenas | '' |
| $strings... | Todas las cadenas que combinaremos | null |

##### Ejemplos

```
@debug lkt-concat(' ', 'Lorem', 'Ipsum', 'Dolor', 'Sit', 'Amet');
```

#### lkt-contains

**STATUS: STABLE**

Comprueba que una lista de Sass contenga una determinada propiedad.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $list | Lista en la que buscar | null |
| $var | Propiedad que buscaremos | null |

##### Ejemplos

```
$list: (1 2 3 4);
@debug lkt-contains($list, 2);
```

#### lkt-implode

**STATUS: STABLE**

Combina todos los valores de una lista en una cadena de texto

| Parámetros | Descripción | Defecto |
|---|---|---|
| $list | Lista para combinar | () |
| $glue | Pegamento de la cadena | '' |

##### Ejemplos

```
$list: (1 2 3 4);
@debug lkt-implode($list, 'hola');
```

#### lkt-is-absolute-length

**STATUS: STABLE**

Comprueba si un valor está en una unidad absoluta: cm, mm, in, px, pt, pc.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a comprobar | null |

##### Ejemplos

```
@debug lkt-is-absolute-length('lorem');
@debug lkt-is-absolute-length(1px);
@debug lkt-is-absolute-length(1.2pt);
```

#### lkt-is-angle

**STATUS: STABLE**

Comprueba si un valor está en una de estas unidades: deg, rad, grad, turn.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a comprobar | null |

##### Ejemplos

```
@debug lkt-is-angle('lorem');
@debug lkt-is-angle(1deg);
@debug lkt-is-angle(1.2rad);
```

#### lkt-is-frequency

**STATUS: STABLE**

Comprueba si un valor está en una frecuencia: Hz, kHz.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a comprobar | null |

##### Ejemplos

```
@debug lkt-is-frequency('lorem');
@debug lkt-is-frequency(1Hz);
@debug lkt-is-frequency(1.2kHz);
```

#### lkt-is-integer

**STATUS: STABLE**

Comprueba si un valor es un número entero.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a comprobar | null |

##### Ejemplos

```
@debug lkt-is-integer('lorem');
@debug lkt-is-integer(1);
@debug lkt-is-integer(1.2);
```

#### lkt-is-length

**STATUS: STABLE**

Comprueba si un valor está en una unidad absoluta o relativa.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a comprobar | null |

##### Ejemplos

```
@debug lkt-is-length('lorem');
@debug lkt-is-length(1%);
@debug lkt-is-length(1.2px);
@debug lkt-is-length(12rem);
```

#### lkt-is-number

**STATUS: STABLE**

Comprueba si un valor es un número.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a comprobar | null |

##### Ejemplos

```
@debug lkt-is-number('lorem');
@debug lkt-is-number(1);
@debug lkt-is-number(1.2);
```

#### lkt-is-percentage

**STATUS: STABLE**

Comprueba si un valor está en una unidad porcentual: %.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a comprobar | null |

##### Ejemplos

```
@debug lkt-is-percentage('lorem');
@debug lkt-is-percentage(1%);
@debug lkt-is-percentage(1.2px);
```

#### lkt-is-relative-length

**STATUS: STABLE**

Comprueba si un valor está en una unidad relativa: em, ex, ch, rem, vw, vh, vmin, vmax.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a comprobar | null |

##### Ejemplos

```
@debug lkt-is-relative-length('lorem');
@debug lkt-is-relative-length(1em);
@debug lkt-is-relative-length(1.2rem);
```

#### lkt-is-resolution

**STATUS: STABLE**

Comprueba si un valor está en una unidad que indica resolución de pantalla: dpi, dpcm, dppx.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a comprobar | null |

##### Ejemplos

```
@debug lkt-is-length('lorem');
@debug lkt-is-length(10dpi);
@debug lkt-is-length(12px);
```

#### lkt-is-time

**STATUS: STABLE**

Comprueba si un valor es una unidad temporal (ms, s).

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a comprobar | null |

##### Ejemplos

```
@debug lkt-is-time('lorem');
@debug lkt-is-time(1ms);
@debug lkt-is-time(1.2s);
```

#### lkt-list-nth-delete

**STATUS: STABLE**

Dada una lista de Sass, elimina el elemento n-ésimo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $list | Lista en la que buscar | null |
| $n | Posición del elemento a eliminar | null |

##### Ejemplos

```
$list: (1 2 3 4);
@debug lkt-list-nth-delete($list, 2);
```

#### lkt-pow

**STATUS: STABLE**

Eleva un número a otro.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $base | Número que elevaremos | null |
| $pow | Potencia a la que elevaremos $base | null |

##### Ejemplos

```
@debug lkt-pow(2, 3);
```

#### lkt-strip-unit

**STATUS: STABLE**

Elimina la unidad de un valor.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor al que eliminar la unidad | '' |

##### Ejemplos

```
@debug lkt-strip-unit(12px);
```

#### lkt-str-replace

**STATUS: STABLE**

En una cadena de texto, reemplaza coincidencias en cadenas de texto por otras.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $haystack | Cadena en la que buscaremos | '' |
| $needle | Texto a buscar | '' |
| $replace | Texto con el que remplazaremos $needle | '' |

##### Ejemplos

```
$str: 'Mi moto alpina derrapante';
$str: lkt-str-replace($str, 'a', 'e');
$str: lkt-str-replace($str, 'i', 'e');
$str: lkt-str-replace($str, 'o', 'e');
@debug $str;
```

#### lkt-to-fixed

**STATUS: STABLE**

Da formato a un número decimal, dejando tantos decimales como queramos.

***Nota***: El límite máximo de decimales es de 5.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $float | Número a formatear | null |
| $digits | Cantidad de decimales | 2 |

##### Ejemplos

```
$a: lkt-to-fixed(1.12345, 2);
```

#### lkt-to-string

**STATUS: STABLE**

Convierte un valor a cadena de texto.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor a convertir | null |

##### Ejemplos

```
$a: lkt-to-string(1);
```

### Mixins

#### lkt-disable-moz-outline

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Deshabilita los estilos de outline de moz

##### Ejemplos

```
button {
    @include lkt-disable-moz-outline();
}
```

#### lkt-disable-webkit-autocomplete

**STATUS: TESTING**

**CONTENT-BLOCK: NO**

Deshabilita los estilos de autocompletado de webkit

##### Ejemplos

```
input,
textarea,
select {
    @include lkt-disable-webkit-autocomplete();
}
```

#### lkt-force-hardware-acceleration

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Fuerza la aceración por hardware.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $use-transform | Indica si agregar un transform. | true |

##### Ejemplos

```
.sample-1 {
    @include lkt-force-hardware-acceleration();
}
```

#### lkt-ms-remove-select-arrow

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Para eliminar los estilos de los selects de IE.

##### Ejemplos

```
@include lkt-ms-remove-select-arrow();
```

#### lkt-placeholder

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Da estilo a los placeholders de forma consistente a través de distintos navegadores.

##### Ejemplos

```
// Global
@include lkt-placeholder(){
    color: red;
}


// Local
.sample-1 {
    @include lkt-placeholder(){
        color: blue;
    }
}
```

#### lkt-out-of-bounds

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Permite a un contenido crecer más allá de los límites de su contenedor.

Cuidado con los overflow: hidden del contenedor.

##### Ejemplos

```
.sample-1 {
    @include lkt-wrap(100px);

    .child {
        @include lkt-out-of-bounds();
    }
}
```

#### lkt-text-overflow-ellipsis

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Un atajo para aplicar la combinación de reglas que muestra text-overflow: ellipsis.

##### Ejemplos

```
.sample-1 {
    @include lkt-text-overflow-ellipsis();
}
```

#### lkt-hyphenate

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Agrega estilos para provocar salto de línea, partiendo la palabra y colocando guiones.

##### Ejemplos

```
.sample-1 {
    @include lkt-hyphenate();
}
```

#### lkt-momentum-scroll

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Agrega estilos para forzar un scroll suave.

##### Ejemplos

```
.sample-1 {
    @include lkt-momentum-scroll();
}
```