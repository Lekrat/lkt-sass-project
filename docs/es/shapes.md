# Api

## Params
| Param | Description |
|---|---|
|$size|Element size|
|$color|Element color|
|content|The mixin accepts sass content directive|

## Mixins: Angles

| Mixin | Description | Params |
|---|---|---|
| lkt-angle-top | Generates an arrow pointing top | $size, $color, content |
| lkt-angle-right | Generates an arrow pointing right | $size, $color, content |
| lkt-angle-bottom | Generates an arrow pointing bottom | $size, $color, content |
| lkt-angle-left | Generates an arrow pointing left | $size, $color, content |

## Mixins: Angle Arrows

| Mixin | Description | Params |
|---|---|---|
| lkt-angle-arrow-top | Generates an arrow pointing top | $size, $color, content |
| lkt-angle-arrow-right | Generates an arrow pointing right | $size, $color, content |
| lkt-angle-arrow-bottom | Generates an arrow pointing bottom | $size, $color, content |
| lkt-angle-arrow-left | Generates an arrow pointing left | $size, $color, content |

## Mixins: Triangles

| Mixin | Description | Params |
|---|---|---|
| lkt-triangle-top | Generates an arrow pointing top | $size, $color, content |
| lkt-triangle-right | Generates an arrow pointing right | $size, $color, content |
| lkt-triangle-bottom | Generates an arrow pointing bottom | $size, $color, content |
| lkt-triangle-left | Generates an arrow pointing left | $size, $color, content |

## Mixins: Geometry

| Mixin | Description | Params |
|---|---|---|
| lkt-circle | Generates a circle | $size, content |
| lkt-square | Generates an square | $size, content |
| lkt-hexagon | Generates an hexagon | $size, $color |