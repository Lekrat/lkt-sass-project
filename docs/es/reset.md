# LKT Reset

## Configuración

El mixin lkt-reset utiliza algunos parámetros de configuración para reiniciar estilos a través de media queries.

- font-size
- font-default

Es una buena idea definir todas las media queries y configurar todos los parámetros a través de ellas antes de ejecutar el mixin.

Para saber más sobre la configuración, echa un vistazo a [LKT Config](./config.md).

## Api

### Mixins

#### lkt-reset

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Reinicia los estilos para eliminar los aplicados por el navegador.

Además, aplica la configuración.

##### Ejemplos

```
// Afecta a todo
@include lkt-reset();

// Afecta a partir de un selector padre
.sample-wrapped {
    @include lkt-reset();
}
```

#### lkt-normalize

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Incluye [normalize.css](https://github.com/necolas/normalize.css).

##### Ejemplos

```
// Afecta a todo
@include lkt-normalize();

// Afecta a partir de un selector padre
.sample-wrapped {
    @include lkt-normalize();
}
```
