# LKT Loaders

## Api

### Mixins

#### lkt-loader-background

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Permite utilizar una imagen de fondo como loader.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $image-url | Ruta hasta la imagen | '' |
| $size | Tamaño del loader | 64px |
| $spin-speed | Velocidad de giro. Si es false no hay giro. | false |

##### HTML

``` 
<div class="lkt-loader-background"></div>
```

##### Uso

```
.lkt-loader-background {
  @include lkt-loader-background('path/ti/image', lkt-rem(80px));
}
```

#### lkt-loader-circle

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-circle"></div>
```

##### Uso

```
.lkt-loader-circle {
  @include lkt-loader-circle(#ffffff);
}
```

#### lkt-loader-default

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
```

##### Uso

```
.lkt-loader-default {
  @include lkt-loader-default(#ffffff);
}
```

#### lkt-loader-dual-ring

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-dual-ring"></div>
```

##### Uso

```
.lkt-loader-dual-ring {
  @include lkt-loader-dual-ring(#ffffff);
}
```

#### lkt-loader-ellipsis

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-ellipsis"><div></div><div></div><div></div><div></div></div>
```

##### Uso

```
.lkt-loader-ellipsis {
  @include lkt-loader-ellipsis(#ffffff);
}
```

#### lkt-loader-facebook

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-facebook"><div></div><div></div><div></div></div>
```

##### Uso

```
.lkt-loader-facebook {
  @include lkt-loader-facebook(#ffffff);
}
```

#### lkt-loader-grid

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-grid"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
```

##### Uso

```
.lkt-loader-grid {
  @include lkt-loader-grid(#ffffff);
}
```

#### lkt-loader-heart

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-heart"><div></div></div>
```

##### Uso

```
.lkt-loader-heart {
  @include lkt-loader-heart(#ffffff);
}
```

#### lkt-loader-hourglass

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-hourglass"></div>
```

##### Uso

```
.lkt-loader-hourglass {
  @include lkt-loader-hourglass(#ffffff);
}
```

#### lkt-loader-ring

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-ring"><div></div><div></div><div></div><div></div></div>
```

##### Uso

```
.lkt-loader-ring {
  @include lkt-loader-ring(#ffffff);
}
```

#### lkt-loader-ripple

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-ripple"><div></div><div></div></div>
```

##### Uso

```
.lkt-loader-ripple {
  @include lkt-loader-ripple(#ffffff);
}
```


#### lkt-loader-roller

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

| Parámetros | Descripción | Defecto |
|---|---|---|
| $color | Color | '' |

##### HTML

```
<div class="lkt-loader-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
```

##### Uso

```
.lkt-loader-roller {
  @include lkt-loader-roller(#ffffff);
}
```

