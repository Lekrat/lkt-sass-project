# How to

```
@include lkt-linear-gradient('to right', (0%: #000000, 10%: #666666, 95%: #AAAAAA, 100%: #FFFFFF));
```

# Api

## Params
| Param | Description |
|---|---|
|$direction|Gradient direction|
|$points|Gradient points|

## Functions

| Function | Description | Params |
|---|---|---|
| lkt-linear-gradient | Transform a list into a linear-gradient string | $direction, $points, $is-repeating |
| lkt-radial-gradient | Transform a list into a radial-gradient string | lkt-radial-gradient($shape, $size, $position, $points, $is-repeating |