# LKT Font

## Configuración

### Tipografía por defecto

Vea cómo establecer la tipografía por defecto en [LKT Config](./config.md#fonts)

### Agregar nuevas tipografías

Siempre tienes la posibilidad de agregar nuevas tipografías a la configuración.

```
$add: lkt-font-add(default, #{"'Lorem Ipsum Dolor', " + sit amet});
$add: lkt-font-add(regular, serif);
```

## Qué hay en la caja

### Tipografías pre cargadas

Vienen pre instaladas las tipografías consideradas como "web safe".

- Serif fonts
  - serif
  - georgia
  - palatino
  - times
- Sans serif
  - sans-serif
  - arial
  - arial-black
  - comic
  - impact
  - lucida
  - tahoma
  - trebuchet
  - verdana
- Monospace fonts
  - monospace
  - courier-new
  - lucida-console

## Api

### Funciones

#### lkt-font-add

**STATUS: STABLE**

Agrega nuevas tipografías a la configuración.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $font-name | El nombre de la fuente. Un alias interno para LKT Font. | '' |
| $value | La tipografía tal y como quieres que se compile en el CSS | '' |

### Mixins

#### lkt-font

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera código CSS relacionado con la tipográfía.

Si la configuración "schema-mode-font" está como true, cada vez que se use lkt-font con los mismos parámetros, irá combinando todas las reglas en una sola.

Hay que señalar que los parámetros cuyo valor sea false no serán generados.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $font-name | El nombre de la fuente configurada usando la función lkt-font-add | false |
| $font-size | El tamaño que tendrá la tipografía | false |
| $line-height | El interlineado | false |
| $font-weight | El peso de la tipografía | false |
| $letter-spacing | El espaciado entre letras | false |
| $font-stretch | El ancho de la letra | false |

##### Ejemplos
```
.sample-1 {
    @include lkt-font(arial, 14px, 120%);
}
.sample-2 {
    @include lkt-font(helvetica, lkt-rem(14px), 120%);
}
.sample-3 {
    @include lkt-font(false, lkt-rem(14px), 120%, 700);
}

```

#### lkt-font-face

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Define nuevos font-faces. Use este mixin solo si tiene la fuente en los siguientes formatos: eot, woff, ttf y svg.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $font-name | El nombre de la fuente | '' |
| $file-name | Nombre del archivo | '' |
| $file-path | Ruta hasta el archivo | '../fonts/' |

##### Ejemplos

```
@include lkt-font-face($name: '', $file: '', $path: '../fonts/');
```

#### lkt-font-face-simple

**STATUS: TESTING**

**CONTENT-BLOCK: NO**

Define nuevos font-faces. Use este mixin si tiene la fuente solo con una extensión.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $font-name | El nombre de la fuente | '' |
| $file-name | Nombre del archivo | '' |
| $file-path | Ruta hasta el archivo | '../fonts/' |
| $weight | Font weight | null |
| $style | Font style | null |

##### Ejemplos

```
@include lkt-font-face-simple($name: '', $file: '', $path: '../fonts/');
```