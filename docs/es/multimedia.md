# LKT Multimedia

## Qué hay en la caja

LKT Multimedia incluye los siguientes ratios de aspecto predefinidos:

- 1:1
- 2:3
- 3:2
- 3:4
- 4:3
- 4:5
- 5:4
- 9:16
- 9:21
- 9:32
- 10:16
- 16:9
- 16:10
- 21:9
- 32:9


## Agregar más alias de ratios de aspecto

Para agregar nuevos alias, simplemente usa la función [lkt-multimedia-add](#lkt-multimedia-add) o [lkt-multimedia-add-custom](#lkt-multimedia-add-custom).

## Api

### Funciones

#### lkt-multimedia-add

**STATUS: STABLE**

Agrega un alias de multimedia basado en un aspect ratio.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $name | Nombre del alias | '' |
| $width | Ancho | '' |
| $height | Alto | '' |

##### Ejemplos
```
$a: lkt-multimedia-add('16:9', 16, 9);
```

#### lkt-multimedia-add-custom

**STATUS: STABLE**

Agrega un alias de multimedia basado en una altura personaliza.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $name | Nombre del alias | '' |
| $value | Valor | '' |

##### Ejemplos
```
$a: lkt-multimedia-add-custom(sample-1, 16%);
$a: lkt-multimedia-add-custom(sample-2, 160px);
```

#### lkt-aspect-ratio-percentage

**STATUS: STABLE**

Calcula el ratio de aspecto entre un ancho y un alto.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $width | Ancho | '' |
| $height | Alto | '' |

##### Ejemplos
```
.sample-1 {
    height: lkt-aspect-ratio-percentage(16, 9) * 1px;
    width: 100%;
}
```

### Mixins


#### lkt-figure

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera estilos para un contenedor multimedia (img, video, iframe, canvas).

| Parámetros | Descripción | Defecto |
|---|---|---|
| $width | Ancho | false |
| $height | Alto | false |
| $mode | Modo. Por defecto: natural. Valores: natural,mask,out,out-x,out-y | false |

##### Ejemplos
```
.sample-1 {
    @include lkt-figure(50%);
}
```

#### lkt-multimedia-fit

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos para un aspect ratio agregado.

El contenido multimedia en su interior se ajustará al ancho y alto del contenedor.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $name | Nombre del alias | '' |

##### Ejemplos
```
.sample-1 {
    @include lkt-multimedia-fit(sample-1);
}
```

#### lkt-multimedia-in

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos para un aspect ratio agregado.

El contenido multimedia en su interior quedará contenido sin sobrepasar el contenedor y sin deformarse.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $name | Nombre del alias | '' |

##### Ejemplos
```
.sample-1 {
    @include lkt-multimedia-in(sample-1);
}
```

#### lkt-multimedia-out

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos para un aspect ratio agregado.

El contenido multimedia en su interior cubrirá todo el contenedor, sin ser deformado, y sobrepasándolo para cubrir el lado menos favorable.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $name | Nombre del alias | '' |

##### Ejemplos
```
.sample-1 {
    @include lkt-multimedia-out(sample-1);
}
```

#### lkt-multimedia-fit-custom

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos para un valor concreto.

El contenido multimedia en su interior se ajustará al ancho y alto del contenedor.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $size | Valor | '' |

##### Ejemplos
```
.sample-1 {
    @include lkt-multimedia-fit-custom(100px);
}
```

#### lkt-multimedia-in-custom

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos para un valor concreto.

El contenido multimedia en su interior quedará contenido sin sobrepasar el contenedor y sin deformarse.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $size | Valor | '' |

##### Ejemplos
```
.sample-1 {
    @include lkt-multimedia-in-custom(10rem);
}
```

#### lkt-multimedia-out-custom

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos para un valor concreto.

El contenido multimedia en su interior cubrirá todo el contenedor, sin ser deformado, y sobrepasándolo para cubrir el lado menos favorable.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $size | Valor | '' |

##### Ejemplos
```
.sample-1 {
    @include lkt-multimedia-out-custom(50vh);
}
```


#### lkt-multimedia-parallax

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos para crear un efecto parallax basado en el tamaño de un alias.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $name | Nombre del alias | '' |

##### Ejemplos
```
.sample-1 {
    @include lkt-multimedia-parallax(sample-1);
    background-image: url("/path/to/img.jpeg");
}
```


#### lkt-multimedia-parallax-custom

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos para crear un efecto parallax basado en un tamaño dado.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $size | Valor | '' |

##### Ejemplos
```
.sample-1 {
    @include lkt-multimedia-parallax-custom(50%);
    background-image: url("/path/to/img.jpeg");
}
```