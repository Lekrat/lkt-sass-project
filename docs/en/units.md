# LKT Units

## Configuración

Toda la configuración se realiza a través de [LKT Config](@todo:link).

## Api

### Funciones

#### lkt-rem

Convierte de px a rem.

Para realizar esta conversión utiliza el valor "unit-factor-rem" de la configuración, aunque puede ser sobrescrito.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-px | Valor inicial en px. | 0 |
| $factor | Factor de conversión a rem | lkt-config(unit-factor-rem) |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-rem) |

#### lkt-em

Convierte de px a em.

Para realizar esta conversión tiene en cuenta un font-size relativo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-px | Valor inicial en px. | 0 |
| $relative-font-size | Font size relativo | 11px |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-em) |

#### lkt-percent

Convierte de px a %.

Para realizar esta conversión tiene en cuenta el ancho del contenedor, en función del cual calcula el porcentage.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-px | Valor inicial en px. | 0 |
| $container-width | Ancho del contenedor | 11px |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-percent) |

#### lkt-vw

Convierte de px a vw.

Para realizar esta conversión tiene en cuenta el ancho del viewport.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-px | Valor inicial en px. | 0 |
| $viewport-width | Ancho del viewport | lkt-config(design-viewport-width) |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-vw) |

#### lkt-vh

Convierte de px a vh.

Para realizar esta conversión tiene en cuenta el alto del viewport.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-px | Valor inicial en px. | 0 |
| $viewport-height | Alto del viewport | lkt-config(design-viewport-height) |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-vh) |

#### lkt-percent-to-px

Convierte de % a px.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-percent | Valor inicial en %. | 0 |
| $container-size | Ancho del contenedor | 0 |

#### lkt-scale-unit

Escala una unidad en función del factor dado.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor inicial. | 0 |
| $factor | Factor | 1 |

#### lkt-unit-grow

Determina cuándo crecerá/decrecerá un valor con el viewport, partiendo de un viewport inicial y un viewport objetivo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $unit-in-px | Valor inicial en px. | 0 |
| $original-viewport-size | Tamaño inicial del viewport | 0 |
| $scaled-viewport-size | Tamaño final del viewport | 0 |

### Mixins

#### lkt-rem

Convierte de px a rem.

Para realizar esta conversión utiliza el valor "unit-factor-rem" de la configuración, aunque puede ser sobrescrito.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | 0 |
| $value-in-px | Valor inicial en px. | 0 |
| $factor | Factor de conversión a rem | lkt-config(unit-factor-rem) |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-rem) |

#### lkt-em

Convierte de px a em.

Para realizar esta conversión tiene en cuenta un font-size relativo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | 0 |
| $value-in-px | Valor inicial en px. | 0 |
| $relative-font-size | Font size relativo | 11px |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-em) |

#### lkt-percent

Convierte de px a %.

Para realizar esta conversión tiene en cuenta el ancho del contenedor, en función del cual calcula el porcentage.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | 0 |
| $value-in-px | Valor inicial en px. | 0 |
| $container-width | Ancho del contenedor | 11px |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-percent) |

#### lkt-vw

Convierte de px a vw.

Para realizar esta conversión tiene en cuenta el ancho del viewport.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | 0 |
| $value-in-px | Valor inicial en px. | 0 |
| $viewport-width | Ancho del viewport | lkt-config(design-viewport-width) |
| $big-enough | Ancho del viewport en el que deja de crecer | 0 |
| $small-enough | Ancho del viewport en el que deja de decrecer | 0 |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-vw) |

#### lkt-vh

Convierte de px a vh.

Para realizar esta conversión tiene en cuenta el alto del viewport.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | 0 |
| $value-in-px | Valor inicial en px. | 0 |
| $viewport-height | Alto del viewport | lkt-config(design-viewport-height)|
| $big-enough | Alto del viewport en el que deja de crecer | 0 |
| $small-enough | Alto del viewport en el que deja de decrecer | 0 |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-vh) |
