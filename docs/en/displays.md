# Api

## Mixins

| Mixin | Description | Params |
|---|---|---|
| lkt-displays | Generates a list of display helpers | $css-prefix, $display-list, $media-queries-list |

## Sample

```
// Declare media queries
$add: lkt-media(only-sm, false, 640px);

// Generate helpers (empty media query is already defined)
@include lkt-displays(is, none block flex inline-flex, '' only-sm);
```

This will generate the following CSS code:

```
.is-none {
  display: none;
}

.is-block {
  display: block;
}

.is-flex {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.is-inline-flex {
  display: -webkit-inline-box;
  display: -ms-inline-flexbox;
  display: inline-flex;
}

@media screen and (max-width: 320px) {
  .is-none-only-sm {
    display: none;
  }

  .is-block-only-sm {
    display: block;
  }

  .is-flex-only-sm {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
  }

  .is-inline-flex-only-sm {
    display: -webkit-inline-box;
    display: -ms-inline-flexbox;
    display: inline-flex;
  }
}
```