# Bienvenid@ a LKT Sass

## Manifiesto del proyecto

### No interferir

Uno de los objetivos de LKT SASS es el de no interferir con otros frameworks.

LKT Sass te provee de una serie de mixins y funciones, pero no genera ningún CSS solo por incluirlo. Eres tú quien decide hasta qué punto quiere usarlo.

### Escribe menos, haz más

Gracias a sus mixins y funciones, LKT Sass se encarga te permite despreocuparte, entre otras cosas:
- de la gestión de variables a través de media queries,
- controlar el crecimiento del contenido automáticamente hasta cierto punto,
- que los elementos multimedia se vean siempre igual
- ¡y mucho más!

### A tu gusto

LKT Sass es totalmente configurable, para que puedas usar las cosas del modo que te gustan.


## Por dónde empezar

Una vez instalado e importado en tu fichero sass principal, simplemente escoge un módulo y lee su documentación.

### Módulos que conforman LKT Sass

Antes de que veas la lista de módulos, deberías saber que existe un módulo de configuración.

Los módulos que lo utilizan tienen un enlace hacia él, pero si quieres echarle un vistazo, lo tienes [aquí](es/config.md).

#### Aligns

Hace que alinear los elementos HTML sea sencillo.

[Saber más](es/aligns.md)

#### Displays

Encargado de generar selectores CSS que funcionan como helpers para controlar la propiedad display a través de diferentes media queries.

[Saber más](es/displays.md)

#### Effects

¿Se te resisten los degradados?

¿Quieres efectos de aparecer y desaparecer?

[Saber más](es/effects.md)

#### Events

Surgió como complemento a LKT Media, para poder aplicar estilos durante un evento a través de media queries de una forma sencilla y fácil de leer.

[Saber más](es/events.md)

#### Grid

En ocasiones, necesitas un sistema de columnas.

En algunas ocasiones, necesitas columnas diferentes a las típicas 12 que tienen todos los frameworks.

En otras ocasiones, quieres poder hacer que unos selectores funcionen exactamente igual que un sistema de columnas.

Y en raras ocasiones, tienes que compatibilizar los selectores CSS de varios frameworks, pero sin generar estilos de más.

[Saber más](es/grid.md)

#### Fonts

Para gestionar tus tipografías.

[Saber más](es/fonts.md)

#### Hamburgers

¿Necesitas un menú de hamburguesa? No busques más.

[Saber más](es/hamburgers.md)

#### Layers

Para gestionar tus z-index.

[Saber más](es/layers.md)

#### Loaders

Loaders predefinidos hechos solo con CSS y la posibilidad de usar los tuyos propios basados en imágenes.

[Saber más](es/loaders.md)

#### Media Queries

La forma definitiva de gestionar y escalar las media queries de tu proyecto.

[Saber más](es/media-queries.md)

#### Multimedia

Aplica estilos a imágenes, vídeos o iframes.

[Saber más](es/multimedia.md)

#### Overlays

Capas con opacidad.

[Saber más](es/overlays.md)

#### Presets

Estilos predefinidos.

Compatibilidad con otros frameworks.

[Saber más](es/presets.md)

#### Reset

Reinicia los estilos del navegador (y es configurable).

[Saber más](es/reset.md)

#### Shapes

¿Alguien necesita una flecha o un triángulo?

[Saber más](es/shapes.md)

#### State

Controla el estado de tus selectores

[Saber más](es/state.md)

#### Tools

Un cajón de sastre lleno de herramientas útiles.

[Saber más](es/tools.md)

#### Units

La forma definitiva de gestionar las unidades.

[Saber más](es/units.md)

#### Wrap

¡Envuelve instantáneamente cualquier elemento!

[Saber más](es/wrap.md)

### Información sobre el estado

En cada función o mixin encontrarás información sobre el estado de su desarrollo.

Los estados son los siguientes:

- STABLE

El componente se considera totalmente desarrollado y funcional y no se preveen cambios en esta versión mayor.

- TESTING

El componente está siendo probado. Esto puede provocar que el componente cambie ligeramente.

- UNSTABLE

El componente está en pleno desarrollo. Hay una base y puede cambiar libremente y/o desaparecer sin previo aviso.

- DEPRECATED

El componente ha sido marcado como obselto y será eliminado en la próxima versión mayor.