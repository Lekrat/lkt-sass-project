# LKT Wrap

## Configuración

Toda la configuración se realiza a través de [LKT Config](.config.md#modo-esquema).

## Api

### Mixins

#### lkt-wrap

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Envuelve el contenido para centrarlo horizontalmente, dejando espacio por los laterales.

El contenido crecerá hasta un máximo indicado por el parámetro $size.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $size | Ancho máximo del elemento. | 0 |

##### Ejemplos
```
.sample-1 {
    @include lkt-wrap(1280px);
}
```

#### lkt-wrap-size

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Permite ajustar el tamaño asignado con lkt-wrap.

Útil para cambiar el tamaño según la media query.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $size | Ancho máximo del elemento. | 0 |

##### Ejemplos
```
.sample-1 {
    @include lkt-wrap(1280px);
    
    @include lkt-media(sm-only){
        @include lkt-wrap-size(320px);
    }
}
```