# Configure

Set default font family:
```
$LKT_DEFAULT_FONT: arial; // Used in reset
```

After import in your project, you can add fonts:

```
$add: lkt-font-add(default, #{"'Lorem Ipsum Dolor', " + sit amet});
$add: lkt-font-add(regular, serif);
```

# Api

## Mixins

This module provides you the following mixins:

| Mixin | Description | Params |
|---|---|---|
| lkt-font | Auto declare and use lkt-schema | $font-name, $font-size, $line-height, $font-weight, $letter-spacing, $font-stretch |


Access font families through mixin:

```
.sample
{
    @include lkt-font(default);
}
```

Access font families through function:

```
.sample
{
    font-family: lkt-font(default);
}
```

## Predefined font stacks:

All web safe fonts are predefined:

- Serif fonts
  - georgia
  - palatino
  - times
- Sans serif
  - arial
  - arial-black
  - comic
  - impact
  - lucida
  - tahoma
  - trebuchet
  - verdana
- Monospace fonts
  - courier-new
  - lucida-console


# Add font faces
```
@include lkt-font-face($name: '', $file: '', $path: '../fonts/');
```