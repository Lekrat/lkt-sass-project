# Configure

After import in your project, you can add layers:

```
$add: lkt-layer-add(default, 1);
$add: lkt-layer-add(modal, 10);
$add: lkt-layer-add(sample, 50);
$add: lkt-layer-add(something, 100);
```

# Api

Access layers through mixin:

```
.sample
{
    @include lkt-layer(sample);
}
```

Access layers through function:

```
.sample
{
    z-index: lkt-layer-get(sample);
}
```