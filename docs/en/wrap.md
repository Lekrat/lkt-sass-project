# Api

## Params
| Param | Description |
|---|---|
| $size | Wrap size in your desired unit (px, rem, em, vw...)|

## Mixins

This package provides you the following mixins:

| Mixin | Description | Params |
|---|---|---|
| lkt-wrap | Generates a wrap container | $size |
| lkt-wrap-size | Change wrap size | $size |

# Samples

```
.container-1{
    @include lkt-wrap(1280px);
}
.container-2{
    @include lkt-wrap(lkt-rem(1280px));
}
```