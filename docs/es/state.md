# LKT State

Para controlar el estado en que se encuentra un selector

## Api

### Mixins

#### lkt-is

**STATUS: STABLE**
**CONTENT-BLOCK: YES**

Indica que el selector tiene una clase de tipo is-$selector.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $selector | Nombre del selector de estado | '' |

##### Ejemplos
```
.sample-1 {
    color: black;
    @include lkt-is(open) {
        color: red;
    }
}
```


#### lkt-child-of

**STATUS: STABLE**
**CONTENT-BLOCK: YES**

Indica que el selector desciende del selector dado.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $selector | Cadena de selectores por encima. **Debe ir con comillas.** | '' |

##### Ejemplos
```
.sample-1 {
    color: black;
    @include lkt-child-of('.main-section .lorem > .ipsum') {
        color: red;
    }
}
```


#### lkt-has

**STATUS: STABLE**
**CONTENT-BLOCK: YES**

Indica que el selector tiene asociado el selector dado.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $selector | Cadena de selectores. **Debe ir con comillas.** | '' |

##### Ejemplos
```
.sample-1 {
    color: black;
    @include lkt-has('.is-active.lorem.ipsum') {
        color: red;
    }
}
```