# Api

## Functions

| Function | Description | Params |
|---|---|---|
| lkt-set | Stores variable | $name, $value |
| lkt-get | Retrieves variable value | $name |