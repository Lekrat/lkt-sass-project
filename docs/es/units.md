# LKT Units

Conceptualmente hablando, LKT Sass está pensando para que trabajes siempre con px.

Pero en el mundo real tenemos que trabajar con más unidades y ahí es donde entra LKT Units.

Diseñado para que puedas mantener todo tu Sass en px y el CSS final esté en rem, em, %, vw o vh.

## Configuración

Toda la configuración se realiza a través de [LKT Config](./config.md#units).

## Api

### Funciones

#### lkt-rem

**STATUS: STABLE**

Convierte de px a rem.

Para realizar esta conversión utiliza el valor "unit-factor-rem" de la configuración, aunque puede ser sobrescrito.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-px | Valor inicial en px. | 0 |
| $factor | Factor de conversión a rem | lkt-config(unit-factor-rem) |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-rem) |

##### Ejemplos
```
.sample-1 {
    width: lkt-rem(100px);
}
.sample-2 {
    width: lkt-rem(100px, 12);
}
.sample-3 {
    width: lkt-rem(100px, 17, 4);
}
.sample-4 {
    width: lkt-rem(100px, 12, 1);
}
.sample-5 {
    padding: lkt-rem(10px 15px, 12, 1);
}
```

#### lkt-em

**STATUS: STABLE**

Convierte de px a em.

Para realizar esta conversión tiene en cuenta un font-size relativo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-px | Valor inicial en px. | 0 |
| $relative-font-size | Font size relativo | 11px |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-em) |

##### Ejemplos

```
.sample-1 {
    width: lkt-em(100px, 16px);
}
.sample-2 {
    width: lkt-em(100px, 12px, 3);
}
```

#### lkt-percent

**STATUS: STABLE**

Convierte de px a %.

Para realizar esta conversión tiene en cuenta el ancho del contenedor, en función del cual calcula el porcentage.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-px | Valor inicial en px. | 0 |
| $container-width | Ancho del contenedor | 11px |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-percent) |

##### Ejemplos

```
.sample-1 {
    width: lkt-percent(100px, 1000px);
}
.sample-2 {
    width: lkt-percent(100px, 1000px, 3);
}
```

#### lkt-vw

**STATUS: STABLE**

Convierte de px a vw.

Para realizar esta conversión tiene en cuenta el ancho del viewport.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-px | Valor inicial en px. | 0 |
| $viewport-width | Ancho del viewport | lkt-config(design-viewport-width) |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-vw) |

##### Ejemplos

```
.sample-1 {
    width: lkt-vw(100px, 1280px);
}
.sample-2 {
    width: lkt-vw(100px, 1280px, 3);
}
```

#### lkt-vh

**STATUS: STABLE**

Convierte de px a vh.

Para realizar esta conversión tiene en cuenta el alto del viewport.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-px | Valor inicial en px. | 0 |
| $viewport-height | Alto del viewport | lkt-config(design-viewport-height) |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-vh) |

##### Ejemplos

```
.sample-1 {
    width: lkt-vh(100px, 720px);
}
.sample-2 {
    width: lkt-vh(100px, 720px, 3);
}
```

#### lkt-percent-to-px

**STATUS: STABLE**

Convierte de % a px.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value-in-percent | Valor inicial en %. | 0 |
| $container-size | Ancho del contenedor | 0 |

##### Ejemplos

```
.sample-1 {
    width: lkt-percent-to-px(37.23%, 1280px);
}
```

#### lkt-percent-through-percent-parent

**STATUS: STABLE**

Calcula el porcentage de un elemento en base no a su padre, sino al su abuelo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $parent-percent | Valor porcentual del padre. | 0 |
| $child-percent | Valor porcentual del hijo | 0 |

##### Ejemplos

```
.sample-1 {
    width: lkt-percent-to-px(37.23%, 1280px);
}
```

#### lkt-scale-unit

**STATUS: STABLE**

Escala una unidad en función del factor dado.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor inicial. | 0 |
| $factor | Factor | 1 |

##### Ejemplos

```
.sample-1 {
    width: lkt-scale-unit(10px, 1.5);
}
.sample-2 {
    width: lkt-scale-unit(10px, .5);
}
```

#### lkt-unit-grow

**STATUS: STABLE**

Determina cuánto crecerá/decrecerá un valor con el viewport, partiendo de un viewport inicial y un viewport objetivo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $unit-in-px | Valor inicial en px. | 0 |
| $original-viewport-size | Tamaño inicial del viewport | 0 |
| $scaled-viewport-size | Tamaño final del viewport | 0 |

##### Ejemplos

```
.sample-1 {
    width: lkt-unit-grow(10px, 1280px, 1920px);
}
.sample-2 {
    width: lkt-unit-grow(10px, 1280px, 640px);
}
```

#### lkt-cast

**STATUS: STABLE**

Permite convertir un valor de una unidad a otra.

No realiza ningún cálculo. Solo cambia la unidad.

Las unidades soportadas son: px, em, rem, vw, vh, %

| Parámetros | Descripción | Defecto |
|---|---|---|
| $value | Valor inicial en cualquier unidad. | 0 |
| $unit | Unidad a la que se desea convertir | 'px' |

##### Ejemplos

```
.sample-1 {
    width: lkt-cast(10px, 'rem');
}
.sample-2 {
    width: lkt-cast(10px, 'vw');
}
```

#### lkt-line-height

**STATUS: STABLE**

Calcula el line-height relativo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $line-height-in-px | Valor en px del line-height deseado | 0 |
| $font-size | Tamaño de la fuente del elemento | 'px' |

##### Ejemplos

```
.sample-1 {
    line-height: lkt-line-height(30px, 25px);
}

// Si hemos configurado la conversión automática:
.sample-2 {
    @include lkt-font(false, 25px, 30px);
}
```

### Mixins

#### lkt-rem

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Convierte de px a rem.

Para realizar esta conversión utiliza el valor "unit-factor-rem" de la configuración, aunque puede ser sobrescrito.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | '' |
| $value-in-px | Valor inicial en px. | 0 |
| $factor | Factor de conversión a rem | lkt-config(unit-factor-rem) |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-rem) |

##### Ejemplos

```
.sample-1 {
    @include lkt-rem(width, 10px)
}
.sample-2 {
    @include lkt-rem(width, 10px, 16)
}
.sample-3 {
    @include lkt-rem(width, 10px, 16, 5)
}
.sample-4 {
    @include lkt-rem(border, 1px solid #000000, 16, 2)
}
.sample-5 {
    @include lkt-rem(height, 20px !important, 16, 2)
}
```

#### lkt-em

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Convierte de px a em.

Para realizar esta conversión tiene en cuenta un font-size relativo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | '' |
| $value-in-px | Valor inicial en px. | 0 |
| $relative-font-size | Font size relativo | 11px |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-em) |

##### Ejemplos

```
.sample-1 {
    @include lkt-em(width, 10px, 16px)
}
.sample-2 {
    @include lkt-em(width, 10px, 16px)
}
.sample-3 {
    @include lkt-em(width, 10px, 16px, 5)
}
.sample-4 {
    @include lkt-em(border, 1px solid #000000, 16px, 2)
}
.sample-5 {
    @include lkt-em(height, 20px !important, 16px, 2)
}
```

#### lkt-percent

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Convierte de px a %.

Para realizar esta conversión tiene en cuenta el ancho del contenedor, en función del cual calcula el porcentage.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | '' |
| $value-in-px | Valor inicial en px. | 0 |
| $container-width | Ancho del contenedor | 11px |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-percent) |

##### Ejemplos

```
.sample-1 {
    @include lkt-percent(width, 10px, 100px)
}
.sample-2 {
    @include lkt-percent(width, 10px, 100px)
}
.sample-3 {
    @include lkt-percent(width, 10px, 100px, 5)
}
.sample-4 {
    @include lkt-percent(border, 1px solid #000000, 100px, 2)
}
.sample-5 {
    @include lkt-percent(height, 20px !important, 100px, 2)
}
.sample-6 {
    @include lkt-percent(height, 40px !important, 100px, 2)
}
.sample-7 {
    @include lkt-percent(height, 40px !important, 100px, 2)
}
```

#### lkt-vw

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Convierte de px a vw.

Para realizar esta conversión tiene en cuenta el ancho del viewport.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | '' |
| $value-in-px | Valor inicial en px. | 0 |
| $viewport-width | Ancho del viewport | lkt-config(design-viewport-width) |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-vw) |

##### Ejemplos

```
.sample-1 {
    @include lkt-vw(width, 10px, 1280px)
}
.sample-2 {
    @include lkt-vw(width, 10px, 1280px)
}
.sample-3 {
    @include lkt-vw(width, 10px, 1280px, 5)
}
.sample-4 {
    @include lkt-vw(border, 1px solid #000000, 1280px, 2)
}
.sample-5 {
    @include lkt-vw(height, 20px !important, 1280px, 2)
}
.sample-6 {
    @include lkt-vw(height, 40px !important, 1280px, 2)
}
.sample-7 {
    @include lkt-vw(height, 40px !important, 1280px, 2)
}
```

#### lkt-vh

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Convierte de px a vh.

Para realizar esta conversión tiene en cuenta el alto del viewport.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | '' |
| $value-in-px | Valor inicial en px. | 0 |
| $viewport-height | Alto del viewport | lkt-config(design-viewport-height) |
| $amount-of-decimals | Cantidad de decimales a redondear | lkt-config(unit-round-vh) |

##### Ejemplos

```
.sample-1 {
    @include lkt-vh(width, 10px, 720px)
}
.sample-2 {
    @include lkt-vh(width, 10px, 720px)
}
.sample-3 {
    @include lkt-vh(width, 10px, 720px, 5)
}
.sample-4 {
    @include lkt-vh(border, 1px solid #000000, 720px, 2)
}
.sample-5 {
    @include lkt-vh(height, 20px !important, 720px, 2)
}
.sample-6 {
    @include lkt-vh(height, 40px !important, 720px, 2)
}
.sample-7 {
    @include lkt-vh(height, 40px !important, 720px, 2)
}
```

#### lkt-big-enough y lkt-small-enough

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Permite indicar que una propiedad crecerá hasta cierto límite.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $property | Propiedad CSS | '' |
| $value-in-px | Valor inicial en px. | 0 |
| $viewport-size | Tamaño del viewport | lkt-config(design-viewport-width) |
| $enough | Alias de [LKT Media](./media-queries.md) en el que dejará de crecer. Alternativamente, el ancho del viewport en el que deja de crecer. | false |
| $unit | Unidad en que se mostrará el valor límite calculado | lkt-config(unit-enough) |

##### Ejemplos
```
.sample-1 {
    @include lkt-vh(width, 10px, 720px)
    
    @include lkt-big-enough(width, 10px, 720px, 1280px, 'px');
    @include lkt-small-enough(width, 10px, 720px, 500px, 'px');
}
```