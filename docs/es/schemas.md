# How to

Let's start adding a basic schema:

```
@include lkt-schema-add(default){
        color: black;
        font-size: 16px;
        line-height: 1.425;
};
```

This will be our basic schema. And we can generate all styles typing this sass. In the future we'll refer this line as "the generation line":

```
@include lkt-schema(default);
```

# Api

## Params
| Param | Description |
|---|---|
|$schema-name|Schema name|

## Mixins

This module provides you the following mixins:

| Mixin | Description | Params |
|---|---|---|
| lkt-schema-add | Generates CSS based on configured schema | $schema-name |
| lkt-schema | Generates CSS based on configured schema | $schema-name |
