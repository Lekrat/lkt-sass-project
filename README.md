# Installation

Execute the next command:

```
npm i git+https://gitlab.com/Lekrat/lkt-sass-project.git --save-dev
```

# Known issues
- After ```npm update``` it's necessary to run ```npm install``` (this package is being uninstalled).

# Usage

## NPM

```
@import "~lkt-sass";
```

## Gulp

In your main.scss:

```
@import "./../path/to/../node_modules/lkt-sass/src/lkt-sass";
```

# See the docs

- [Spanish](./docs/home-es.md)
- [English (WIP)](./docs/home-es.md)
