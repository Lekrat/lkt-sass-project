# LKT Effects

Módulo dedicado a simplificar efectos.

## Api

### Funciones

#### lkt-linear-gradient

**STATUS: STABLE**

Genera un degradado linear.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $direction | Dirección. | 'to right' |
| $points | A SASS list of points. | () |
| $is-repeating | Indica si es un degradado que se repite | false |

##### Ejemplos
```
.sample-1 {
    @include lkt-linear-gradient('to right', (0%: #000000, 10%: #666666, 95%: #AAAAAA, 100%: #FFFFFF));
}
```

#### lkt-radial-gradient

**STATUS: STABLE**

Genera un degradado radial.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $shape | Forma. | 'ellipse' |
| $size | Tamaño. | 'farthest-corner' |
| $position | Posición. | 'center' |
| $points | A SASS list of points. | () |
| $is-repeating | Indica si es un degradado que se repite | false |

##### Ejemplos
```
.sample-1 {
    @include lkt-radial-gradient(ellipse, fartherst-corner, center, (0%: #000000, 10%: #666666, 95%: #AAAAAA, 100%: #FFFFFF));
}
```


### Mixins

#### lkt-animate-appear

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos necesarios para animar un elemento apareciendo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $duration | Duración de la animación | 250ms |
| $timing | Función de tiempo | cubic-bezier(.36, .07, .19, .97) |
| $delay | Retardo de la animación | false |

##### Ejemplos

```
// HTML
<div class="sample-1">
    <p>Pasa por encima</p>
    <div>Hola</div>
</div>

// SCSS
.sample-1 {
    div {
        display: none;
    }

    p:hover + div {
        @include lkt-animate-appear();
    }

}
```

#### lkt-animate-disappear

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos necesarios para animar un elemento apareciendo.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $duration | Duración de la animación | 250ms |
| $timing | Función de tiempo | cubic-bezier(.36, .07, .19, .97) |
| $delay | Retardo de la animación | false |

##### Ejemplos

```
// HTML
<div class="sample-1">
    <p>Pasa por encima</p>
    <div>Hola</div>
</div>

// SCSS
.sample-1 {
    p:hover + div {
        @include lkt-animate-disappear();
    }

}
```

#### lkt-animate-scale

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos necesarios para animar un elemento encogiendo y volviendo a crecer.

Para cualquier parámetro, el valor "initial" usa el valor por defecto.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $duration | Duración de la animación | 250ms |
| $timing | Función de tiempo | cubic-bezier(.36, .07, .19, .97) |
| $delay | Retardo de la animación | false |
| $transform-fix | Agrega propiedades transform para agregar a la animación | '' |

##### Ejemplos

```
// HTML
<div class="sample-1">
    <p>Pasa por encima</p>
    <div>Hola</div>
</div>

// SCSS
.sample-1 {
    p:hover + div {
        @include lkt-animate-scale();
    }

}

// SCSS
.sample-2 {
    p:hover + div {
    @include lkt-animate-scale(initial, initial, initial, translateY(-50%));
    }

}
```

#### lkt-shadow-3d-right-top

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera una sombra que genera efecto de proyección.

| Parámetros | Descripción | Defecto |
|---|---|---|
|$size-in-px|Tamaño de la sombra. Solo admite px | 5px |
|$color|Color de la sombra | #000 |

##### Ejemplos

```
.sample-1 {
    @include lkt-shadow-3d-right-top(10px, #000);
}
```

#### lkt-shadow-3d-right-bottom

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera una sombra que genera efecto de proyección.

| Parámetros | Descripción | Defecto |
|---|---|---|
|$size-in-px|Tamaño de la sombra. Solo admite px | 5px |
|$color|Color de la sombra | #000 |

##### Ejemplos

```
.sample-1 {
    @include lkt-shadow-3d-right-bottom(10px, #000);
}
```

#### lkt-shadow-3d-left-top

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera una sombra que genera efecto de proyección.

| Parámetros | Descripción | Defecto |
|---|---|---|
|$size-in-px|Tamaño de la sombra. Solo admite px | 5px |
|$color|Color de la sombra | #000 |

##### Ejemplos

```
.sample-1 {
    @include lkt-shadow-3d-left-top(10px, #000);
}
```

#### lkt-shadow-3d-left-bottom

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera una sombra que genera efecto de proyección.

| Parámetros | Descripción | Defecto |
|---|---|---|
|$size-in-px|Tamaño de la sombra. Solo admite px | 5px |
|$color|Color de la sombra | #000 |

##### Ejemplos

```
.sample-1 {
    @include lkt-shadow-3d-left-bottom(10px, #000);
}
```


#### lkt-effect-underline

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Genera los estilos necesarios para realizar un efecto de subrayado creciente.

Este mixin prepara los estilos, pero no prepara el evento. 

Puedes incrementar la separación entre el elemento y el subrayado aumentando el padding-bottom.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $origin | Punto de inicio. Valores posibles: left, center, right | left |
| $color | Color del subrayado | #000 |
| $weight | Grosor del subrayado | 1px |
| $initial-width | Ancho inicial del subrayado | 0 |
| $pseudo | Pseudo elemento a utilizar. Valores posibles: before, after | before |
| $bottom | Propiedad bottom | 0 |
| $transition | Transición | width linear 250ms |

##### Ejemplos

```
.sample-1 {
    @include lkt-effect-underline(left, #000, 1px);
}
.sample-2 {
    @include lkt-effect-underline(center, #000, 2px);
}
.sample-3 {
    @include lkt-effect-underline(right, #000, 3px);
}
```

#### lkt-effect-underline-triggered

**STATUS: STABLE**

**CONTENT-BLOCK: NO**

Dispara el efecto de subrayado.

| Parámetros | Descripción | Defecto |
|---|---|---|
| $final-width | Ancho final del subrayado | 100% |
| $pseudo | Pseudo elemento a utilizar. Valores posibles: before, after | before |

##### Ejemplos

```
.sample-1 {
    @include lkt-effect-underline(left, #000, 1px);
    
    @include lkt-hover(md){
        @include lkt-effect-underline-triggered();
    }
}
.sample-2 {
    @include lkt-effect-underline(center, #000, 2px);
    
    @include lkt-hover(md){
        @include lkt-effect-underline-triggered();
    }
}
.sample-3 {
    @include lkt-effect-underline(right, #000, 3px);
    
    @include lkt-hover(md){
        @include lkt-effect-underline-triggered();
    }
}
```