# Third party libraries which was forked (alphabetical sort)

## hamburgers

Hamburger library (1.1.3) was forked since LKT 1.3.

Thanks to [Jonsuh](https://github.com/jonsuh/hamburgers)

## Pure CSS Loaders

Pure CSS Loaders was forked since LKT 1.3.

Thanks to [Pure CSS Loaders](https://loading.io/css/)

## normalize.css

Normalize.css library (8.0.1) was forked since LKT 1.0

Thanks to [normalize.css team](https://necolas.github.io/normalize.css)
